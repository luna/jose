#!/usr/bin/env python3

import json
import subprocess
import asyncio
import logging

from .common import Cog
from discord.ext import commands

log = logging.getLogger(__name__)


def pip_freeze():
    """call pip freeze, get results"""
    out = subprocess.check_output("pipenv run pip freeze", shell=True)
    return out


class PipUpdates(Cog):
    def __init__(self, bot):
        super().__init__(bot)

        self.watch = {}
        self.requirements = {}
        self.update_task = None

    def cog_load(self):
        piplock = json.load(open("Pipfile.lock", "r"))
        for pkgname, pkg in piplock["default"].items():
            if pkg.get("index") != "pypi":
                continue

            pkgversion = pkg["version"]

            if not pkgversion.startswith("=="):
                continue

            self.requirements[pkgname] = pkg["version"].lstrip("==")

        self.update_task = self.bot.loop.create_task(self.update_task_func())

    def cog_unload(self):
        self.update_task.cancel()

    async def update_task_func(self):
        try:
            while True:
                await self.checkupdates()
                await asyncio.sleep(21600)
        except asyncio.CancelledError:
            pass
        except:
            log.exception("shit")

    async def checkupdates(self):
        future_pip = self.bot.loop.run_in_executor(None, pip_freeze)
        out = await future_pip
        out = out.decode("utf-8")
        packages = out.split("\n")

        res = []

        for pkgline in packages:
            r = pkgline.split("==")
            if len(r) != 2:
                continue
            pkgname = r[0]

            if pkgname in self.requirements:
                cur_version = self.requirements[pkgname]

                pkgdata = await self.get_json(f"https://pypi.org/pypi/{pkgname}/json")
                new_version = pkgdata["info"]["version"]

                if new_version != cur_version:
                    res.append(
                        " * `%r` needs update from %s to %s"
                        % (pkgname, cur_version, new_version)
                    )

        await self.say_results(res)
        return res

    async def say_results(self, res):
        if len(res) <= 0:
            return

        res.insert(0, ":alarm_clock: You have package updates :alarm_clock:")
        await self.bot.owner.send("\n".join(res))

    @commands.command(hidden=True)
    @commands.is_owner()
    async def checkpkgs(self, ctx):
        """Query PyPI for new package updates."""
        async with ctx.typing():
            res = await self.checkupdates()

            if len(res) <= 0:
                return await ctx.send("`No updates found.`")

            await ctx.send("Updates were found and should be " "sent to the bot owner!")


async def setup(bot):
    await bot.add_cog(PipUpdates(bot))
