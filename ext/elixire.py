import time

import discord

from discord.ext import commands

from .common import Cog


def to_emoj(val: bool):
    return "\N{OK HAND SIGN}" if val else "\N{CROSS MARK}"


def to_ago_ts(sv_data) -> float:
    now = int(time.time() * (10 ** 3))

    # convert back to seconds
    latency_ago = (now - sv_data["timestamp"]) / (10 ** 3)
    latency_ago = round(latency_ago, 1)

    return latency_ago


class Elixire(Cog):
    """Commands for elixi.re"""

    ELSTAT = "https://status.lavatech.top"

    @commands.command(aliases=["eping", "ep"])
    async def elixstatus(self, ctx):
        """Query elstat instance and fetch data"""
        async with ctx.typing():
            quick_json = await self.get_json(f"{self.ELSTAT}/api/quick")

        embed = discord.Embed()

        services = quick_json["status"].keys()

        for service in services:
            sv_data = quick_json["status"][service]

            sv_uptime = float(quick_json["uptime"][service])
            sv_uptime = round(sv_uptime, 5)

            latency = sv_data.get("latency")

            if latency is None:
                continue

            latency = round(latency)
            latency_ago = to_ago_ts(sv_data)

            embed.add_field(
                name=f"{service} ({sv_uptime}%)",
                value=f'status: {to_emoj(sv_data["status"])}\n'
                f"latency: {latency}ms, {latency_ago}s ago",
            )

        await ctx.send(embed=embed)

    @commands.command()
    async def rlmest(self, ctx):
        """query rlme lmao"""
        async with ctx.typing():
            quick_json = await self.get_json(f"https://rlme-is-shit.l4.pm/api/quick")

        sv_data = quick_json["status"]["ratelimited"]

        # get uptimes
        day_uptime = quick_json["uptime"]["ratelimited"]
        week_uptime = quick_json["week_uptime"]["ratelimited"]
        mon_uptime = quick_json["month_uptime"]["ratelimited"]

        latency_sec = to_ago_ts(sv_data)

        await ctx.send(
            f"fuck rl me\n"
            f' - status: {to_emoj(sv_data["status"])}\n'
            f' - latency: {sv_data["latency"]}ms, {latency_sec}s ago\n'
            f" - daily uptime: {day_uptime}%\n"
            f" - weekly uptime: {week_uptime}%\n"
            f" - monthly uptime: {mon_uptime}%\n"
        )

    @commands.command()
    async def dstats(self, ctx):
        """Get counts about elixi.re's domain list."""
        async with ctx.typing():
            domain_json = await self.get_json("https://elixi.re/api/domains")

        domain_count = len(domain_json["domains"])
        official_count = len(domain_json["officialdomains"])

        if domain_count > 69:
            domain_count = f"69 (real: {domain_count})"

        await ctx.send(
            f"there are {domain_count} domains\n"
            f"out of those, {official_count} domains are official."
        )


async def setup(bot):
    await bot.add_cog(Elixire(bot))
