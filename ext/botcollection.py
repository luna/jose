import logging

import discord
from discord.ext import commands

from .common import Cog

BOT_RATIO_MIN = 1.7
BOT_RATIO_MAX = 1.1

WHITELIST = (
    273863625590964224,  # José's server
    295341979800436736,  # Memework
    319540379495956490,  # v2 testing serv
    380295555039100932,  # Comet Obversavotory / luma's home
    319252487280525322,  # robert is gay
    340609473439596546,  # slice is a furry that plays agario
    191611344137617408,  # dan's 'haha gay pussy party'
    277919178340565002,  # lold - lolbot testing server
    248143597097058305,  # cyn private server
    291990349776420865,  # em's meme heaven
    366513799404060672,  # dan's another gay guild
    322885030806421509,  # heating's gay guild
    422495103941345282,  # muh testing
    466088422851870720,  # carl letter emotes
    471075547104018432,  # cyn server 2
    584657503560925194,  # personal
    720020173310132348,  # catgirl cabal
)

log = logging.getLogger(__name__)


class BotCollection(Cog):
    """Bot collection commands."""

    def bot_human_ratio(self, guild):
        """Return the ratio numbers for a given guild"""
        bots = sum(member.bot for member in guild.members)
        humans = guild.member_count - bots

        return bots, humans, bots / humans

    def bhratio_global(self):
        """Return the average bot ratio across all guilds."""
        all_members = self.bot.get_all_members

        bots = sum(member.bot for member in all_members())
        total_members = sum(guild.member_count for guild in self.bot.guilds)
        humans = total_members - bots

        return bots, humans, (bots / humans)

    async def guild_ratio(self, guild: discord.Guild) -> float:
        """Get the bot-to-human ratio for a guild"""
        if len(guild.members) < 50:
            return BOT_RATIO_MIN

        return BOT_RATIO_MAX

    def fallback(self, guild: discord.Guild, message: str):
        """Send a message to the first channel we can send.

        Serves as a fallback instad of DMing owner.
        """
        chan = next(
            c for c in guild.text_channels if c.permissions_for(guild.me).send_messages
        )
        return chan.send(message)

    @Cog.listener()
    async def on_guild_join(self, guild):
        """Main bot collection checker."""
        bots, humans, ratio = self.bot_human_ratio(guild)
        owner = guild.owner

        log.info(f"[bh:join] {guild!s} -> ratio {bots}b / {humans}h " f"= {ratio:.2}")

        if guild.id in WHITELIST:
            return

        # ignore when owner
        if guild.owner.id == self.bot.owner_id:
            return

        bot_ratio = await self.guild_ratio(guild)
        if ratio > bot_ratio:
            log.info(
                f"[bh:leave:guild_join] {ratio} > {bot_ratio}," f" leaving {guild!s}"
            )

            explode_bh = (
                "This guild was classified as a bot collection, "
                "josé automatically left. "
                f"`{ratio} > {bot_ratio}`.\n"
                "Remove bots from the guild or add more people to"
                "decrease your ratio."
            )
            try:
                await owner.send(explode_bh)
            except discord.Forbidden:
                await self.fallback(guild, explode_bh)

            return await guild.leave()

        if await self.bot.is_blocked_guild(guild.id):
            blocked_msg = (
                "Sorry. The guild you added José on is blocked. "
                "Appeal to the block at the support server"
                "(Use the invite provided in `j!invite`)."
            )

            try:
                await owner.send(blocked_msg)
            except discord.Forbidden:
                await self.fallback(guild, blocked_msg)

            return await guild.leave()

        welcome = (
            "Hello, welcome to José!\n"
            "Discord's API Terms of Service requires me to"
            " tell you I log\n"
            "Command usage and errors to a special channel.\n"
            "**Only commands and errors are logged, no "
            "messages are logged, ever.**\n"
            "**Disclaimer:** José is free and open source software"
            " maintained by the hard work of many volunteers.\n"
            "\n**SPAM IS NOT TOLERATED.**"
        )

        try:
            await owner.send(welcome)
        except discord.Forbidden:
            await self.fallback(guild, welcome)

    @Cog.listener()
    async def on_member_join(self, member):
        guild = member.guild

        if guild.id in WHITELIST:
            return

        bots, humans, ratio = self.bot_human_ratio(guild)
        bot_ratio = await self.guild_ratio(guild)

        if ratio > bot_ratio:
            log.info(
                f"[bh:leave:member_join] leaving {guild!r} {guild.id},"
                f" {ratio} ({bots} / {humans}) > {bot_ratio}"
            )

            bc_msg = (
                "Your guild became classified as a bot"
                "collection, josé automatically left."
                f"{bots} bots, "
                f"{humans} humans, "
                f"{ratio}b/h > {bot_ratio}"
            )

            try:
                await guild.owner.send(bc_msg)
            except discord.Forbidden:
                await self.fallback(guild, bc_msg)

            await guild.leave()

    @commands.command()
    @commands.guild_only()
    async def bhratio(self, ctx):
        """Get your guild's bot-to-human ratio"""

        bots, humans, ratio = self.bot_human_ratio(ctx.guild)
        _, _, global_ratio = self.bhratio_global()

        await ctx.send(
            f"{bots} bots / {humans} humans => "
            f"`{ratio:.2}b/h`, global is `{global_ratio:.2}`"
        )


async def setup(bot):
    await bot.add_cog(BotCollection(bot))
