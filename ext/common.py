import re
import asyncio
import decimal
import logging
import io
from typing import Tuple
from collections import namedtuple

import discord
from discord.ext import commands

JOSE_VERSION = "2.4"

ZERO = decimal.Decimal(0)
INF = decimal.Decimal("inf")

WIDE_MAP = dict((i, i + 0xFEE0) for i in range(0x21, 0x7F))
WIDE_MAP[0x20] = 0x3000

log = logging.getLogger(__name__)


class SayException(Exception):
    """Say something using an exception."""

    pass


class GuildConverter(commands.Converter):
    """Convert the name of a guild to
    a Guild object."""

    async def convert(self, ctx, arg):
        bot = ctx.bot

        try:
            guild_id = int(arg)
        except ValueError:

            def is_guild(g):
                return arg.lower() == g.name.lower()

            guild = discord.utils.find(is_guild, bot.guilds)

            if guild is None:
                raise commands.BadArgument("Guild not found")
            return guild

        guild = bot.get_guild(guild_id)
        if guild is None:
            raise commands.BadArgument("Guild not found")
        return guild


class CoinConverter(commands.Converter):
    """Does simple checks to the value being given.

    Also checks if the user has an account
    """

    async def convert(self, ctx, argument):
        ba = commands.BadArgument
        coins = ctx.bot.get_cog("Coins")
        if not coins:
            raise ba("Coins extension not loaded.")

        if argument.lower() == "all":
            if ctx.invoked_with in ("steal", "heist"):
                # this is the member/guild
                target = ctx.args[-1]
            else:
                target = ctx.author

            try:
                account = await coins.get_account(target.id)
            except coins.AccountNotFoundError:
                raise ba(f"Your target `{target}` does not have a" " JoséCoin account.")

            return account["amount"]

        value = decimal.Decimal(argument)
        if value <= ZERO:
            raise ba("You can't input values lower or equal to 0.")
        elif value >= INF:
            raise ba("You can't input values equal or higher to infinity.")

        try:
            value = round(value, 2)
        except:
            raise ba("Rounding failed.")

        # Ensure a taxbank account tied to the guild exists
        await coins.ensure_taxbank(ctx)
        try:
            account = await coins.get_account(ctx.author.id)
        except coins.AccountNotFoundError:
            raise ba("You don't have a JoséCoin account, " f"make one with `j!account`")

        return value


class MaybeUser(commands.Converter):
    async def convert(self, ctx, arg) -> int:
        try:
            other = discord.ext.commands.UserConverter()
            user = await discord.ext.commands.UserConverter.convert(other, ctx, arg)
            return user.id
        except commands.BadArgument:
            return int(arg)


class FuzzyMember(commands.Converter):
    """Fuzzy matching for member objects"""

    async def convert(self, ctx, arg):
        arg = arg.lower()
        ms = ctx.guild.members
        scores = {}
        for m in ms:
            score = 0
            mn = m.name

            # compare against username
            # We give a better score to exact matches
            # than to just "contain"-type matches
            if arg == mn:
                score += 10
            if arg in mn:
                score += 3

            # compare with nickname in a non-throw-exception way
            nick = getattr(m.nick, "lower", "".lower)()
            if arg == nick:
                score += 2
            if arg in nick:
                score += 1

            # we don't want a really big dict thank you
            if score > 0:
                scores[m.id] = score

        try:
            sortedkeys = sorted(scores.keys(), key=lambda k: scores[k], reverse=True)
            return sortedkeys[0]
        except IndexError:
            raise commands.BadArgument("No user was found")


MESSAGE_SPECIFIER_RE = re.compile(r"(\d+)?(?:[:+](-?\d+))?")


class Specifier(namedtuple("Specifier", ["id", "range"])):
    range_limit = 50

    @classmethod
    def from_string(cls, specifier):
        match = MESSAGE_SPECIFIER_RE.match(specifier)

        (message_id, m_range) = match.groups()

        if not message_id and not m_range:
            return None

        def convert(string):
            return None if string is None else int(string)

        return cls(
            id=convert(message_id),
            range=convert(m_range),
        )

    @property
    def relative(self):
        return self.id is None and self.range

    @property
    def over_limit(self):
        return self.range < -self.range_limit or self.range > self.range_limit


class QuoteName(commands.Converter):
    def __init__(
        self,
        *,
        must_exist: bool = False,
        must_not_exist: bool = False,
    ):
        super().__init__()
        self.must_exist = must_exist
        self.must_not_exist = must_not_exist

    async def convert(self, ctx, argument):
        quotes = ctx.cog.quotes(ctx.guild)

        # scrub any mentions
        argument = clean_mentions(ctx.channel, argument)

        if argument not in quotes:
            if self.must_exist:
                raise commands.BadArgument(f'Quote "{argument}" does not exist.')

        if argument in quotes and self.must_not_exist:
            raise commands.BadArgument(f'Quote "{argument}" already exists.')

        if len(argument) > 60:
            raise commands.BadArgument("Too long. 60 characters maximum.")

        return argument


def stringify_message(message: discord.Message) -> str:
    content = message.clean_content

    if message.attachments:
        urls = [attachment.url for attachment in message.attachments]
        content += f' {" ".join(urls)}'

    if message.embeds:
        content += f" ({len(message.embeds)} embeds)"

    return f"<{message.author.name}> {content}"


class Messages(commands.Converter):
    """A converter that is able to resolve a message or range of messages."""

    async def convert(self, ctx, argument):
        spec = Specifier.from_string(argument)

        if not spec:
            raise commands.BadArgument(
                f"Invalid message ID. See `{ctx.prefix}help quote`."
            )

        if spec.range:
            if spec.over_limit:
                raise commands.BadArgument(
                    f"Ranges can only target up to {Specifier.range_limit} messages."
                )

            if not spec.relative and spec.range < 1:
                raise commands.BadArgument("Range should be greater than 1.")
            elif spec.relative and spec.range > -1:
                raise commands.BadArgument("Range should be less than -1.")

        try:
            if spec.relative:
                # relative to the sent message instead of a message id, get the
                # last n messages. have to get +1 because history will get the
                # command message too.
                history = [m async for m in ctx.history(limit=abs(spec.range) + 1)]
                rev = reversed(history[1:])
                return list(rev)

            message = await ctx.fetch_message(spec.id)

            if spec.range:
                # relative to the specified message id, get the message and
                # n messages after
                after_messages = [
                    m
                    async for m in ctx.history(
                        after=discord.Object(id=spec.id),
                        limit=spec.range,
                    )
                ]
                return [message] + after_messages

            return message
        except discord.NotFound as error:
            raise commands.BadArgument(f"Not found: {error}")
        except discord.HTTPException as error:
            raise commands.BadArgument(f"Failed to get message(s): {error}")


class Cog(commands.Cog):
    """Main cog base class.

    Provides common functions to cogs.
    """

    def __init__(self, bot):
        self.bot = bot
        self.loop = bot.loop
        self.JOSE_VERSION = JOSE_VERSION

        # so it becomes available for all cogs without needing to import shit
        self.SayException = SayException
        self.prices = {"OPR": 0.9, "API": 0.65, "TRN": "0.022/char"}

    def __init_subclass__(cls, **kwargs):
        """Fill in cog metadata about a cog's requirements."""
        requires = kwargs.get("requires", [])

        cls.cog_metadata = {"requires": requires}

    async def get_json(self, url: str) -> "any":
        """Get JSON from a url."""
        async with self.bot.session.get(url) as resp:
            try:
                return await resp.json()
            except Exception as err:
                raise SayException(f"Error parsing JSON: {err!r}")

    async def http_get(self, url):
        async with self.bot.session.get(url) as resp:
            return await resp.text()

    async def http_read(self, url):
        async with self.bot.session.get(url) as resp:
            return io.BytesIO(await resp.read())

    @property
    def config(self):
        return self.bot.cogs.get("Config")

    @property
    def jcoin(self):
        return self.bot.cogs.get("Coins")

    @property
    def coins(self):
        return self.bot.cogs.get("Coins")

    @property
    def pool(self):
        return self.bot.cogs.get("Config").db

    @property
    def pool_available(self):
        return self.bot.cogs.get("Config").db_available


async def shell(command: str, *, truncate: bool = False) -> Tuple[int, str]:
    """Execute shell commands."""
    process = await asyncio.create_subprocess_shell(
        command,
        stderr=asyncio.subprocess.STDOUT,  # lump stderr into stdout
        stdout=asyncio.subprocess.PIPE,
    )

    output = (await process.communicate())[0].decode().strip()

    if truncate and len(output) > 1500:  # arbitrary
        output = f"{output[:1500]}..."

    return process.returncode, output
